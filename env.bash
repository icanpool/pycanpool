#!/usr/bin/env bash
# Copyright (c) 2021-2022 maminjie <canpool@163.com>
# SPDX-License-Identifier: MulanPSL-2.0
# Config environment before develop, please run: source env.bash

root_path=$(cd $(dirname ${BASH_SOURCE}); pwd)
python_paths=$(echo ${PYTHONPATH} | sed 's/:/ /g')
existed=0
for path in $python_paths; do
	if [[ "$root_path" = "$path" ]]; then
		existed=1; break
	fi
done

if [[ $existed -eq 0 ]]; then
    user_path=${root_path}:${root_path}/projects
    if [[ -z "${PYTHONPATH}" ]]; then
	    export PYTHONPATH=${user_path}
    else
        export PYTHONPATH=${PYTHONPATH}:${user_path}
    fi
fi
echo "PYTHONPATH=${PYTHONPATH}"
