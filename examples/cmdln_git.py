#!/usr/bin/env python3
# Copyright (c) 2021-2023 maminjie <canpool@163.com>
# SPDX-License-Identifier: MulanPSL-2.0

"""A demonstration for how one would start implementing 'git' (the
version control system command-line client) using cmdln.py.
"""

import sys

from pycanpool import cmdln


class MyGIT(cmdln.Cmdln):
    """
    Git is a free and open source distributed version control system.
    Type 'git <subcommand> --help' for help on a specific subcommand.

    Most subcommands take file and/or directory arguments, recursing
    on the directories.  If no arguments are supplied to such a
    command, it will recurse on the current directory (inclusive) by
    default.
    """
    name = "git"

    def __init__(self):
        cmdln.Cmdln.__init__(self)
        cmdln.Cmdln.do_help.aliases.append("h")

    @cmdln.alias("it", "ini")
    @cmdln.option("-q", "--quiet", action='store_true',
                  help='be quiet')
    @cmdln.option("--bare", action='store_true',
                  help='create a bare repository')
    @cmdln.option("--template", type=str, metavar="<template-directory>",
                  help='directory from which templates will be used')
    @cmdln.option("--shared", metavar='[<permissions>]',
                  help='specify that the git repository is to be shared amongst several users')
    def do_init(self, subcmd, opts, *args):
        """
        Create an empty Git repository or reinitialize an existing one

        other messages
        """
        print("'git %s' opts: %s" % (subcmd, opts))
        print("'git %s' args: %s" % (subcmd, args))


if __name__ == "__main__":
    mygit = MyGIT()
    sys.exit(mygit.main())
