#!/usr/bin/env python3
# Copyright (c) 2024 maminjie <canpool@163.com>
# SPDX-License-Identifier: MulanPSL-2.0

import scrapy
import json


class SecurityBulletinSpider(scrapy.Spider):
    name = "security_bulletin"
    allowed_domains = ["www.openeuler.org"]
    # see https://www.openeuler.org/zh/security/security-bulletins/
    start_urls = ["https://www.openeuler.org/api-euler/api-cve/cve-security-notice-server/securitynotice/findAll"]

    def start_requests(self):
        start_url = self.start_urls[0]
        for num in range(1, 300):
            data = {
                "pages": {
                    "page": num,
                    "size": 10
                },
                "keyword": "",
                "type": [],
                "date": [],
                "affectedProduct": [],
                "affectedComponent": "",
                "noticeType": "cve"
            }
            yield scrapy.FormRequest(
                method="POST",
                url=start_url,
                body=json.dumps(data),
                callback=self.parse_list,
                dont_filter=True,
            )

    def parse_list(self, response):
        security_notice_lst = json.loads(response.text)["result"]["securityNoticeList"]
        detail_url = "https://www.openeuler.org/api-euler/api-cve/cve-security-notice-server/securitynotice/getBySecurityNoticeNo?securityNoticeNo={}"
        for security_notice in security_notice_lst:
            item = {
                "securityNoticeNo": security_notice["securityNoticeNo"],
                "summary": security_notice["summary"],
                "type": security_notice["type"],
                "affectedProduct": security_notice["affectedProduct"],
                "affectedComponent": security_notice["affectedComponent"],
                "updateTime": security_notice["updateTime"],
                'cveId': security_notice['cveId']
            }
            yield scrapy.Request(
                method="GET",
                url=detail_url.format(item["securityNoticeNo"]),
                callback=self.parse_detail,
                dont_filter=True,
                meta={"item": item}
            )

    def parse_detail(self, response):
        item = response.meta["item"]
        package_helper_lst = json.loads(response.text)["result"]["packageHelperList"]
        for package_helper in package_helper_lst:
            if package_helper["productName"] == "openEuler-22.03-LTS-SP1":
                child_lst = package_helper["child"]
                for child in child_lst:
                    system_arch = child["productName"]
                    item[system_arch] = []
                    rpm_lst = child["child"]
                    for rpm in rpm_lst:
                        rpm_pack_name = rpm["packageName"]
                        item[system_arch].append(rpm_pack_name)
        yield item
