# Define your item pipelines here
#
# Don't forget to add your pipeline to the ITEM_PIPELINES setting
# See: https://docs.scrapy.org/en/latest/topics/item-pipeline.html


import csv


class OpeneulerPipeline:
    def open_spider(self, spider):
        if spider.name == "security_bulletin":
            self.f = open('openeuler_security_bulletins.csv', 'w', encoding='utf-8', newline="")
            self.csv_write = csv.writer(self.f)

    def close_spider(self, spider):
        if spider.name == "security_bulletin":
            self.f.close()

    def process_item(self, item, spider):
        if spider.name == "security_bulletin":
            self.csv_write.writerow(
                [item["securityNoticeNo"],
                 item["summary"],
                 item["type"],
                 item["affectedProduct"],
                 item["affectedComponent"],
                 item["updateTime"],
                 item['cveId'].rstrip(';'),
                 ";".join(item.get("src", [])),
                 ";".join(item.get("noarch", [])),
                 ";".join(item.get("aarch64", [])),
                 ";".join(item.get("x86_64", [])), ]
            )
