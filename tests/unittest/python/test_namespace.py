#!/usr/bin/env python3
# Copyright (c) 2024 maminjie <canpool@163.com>
# SPDX-License-Identifier: MulanPSL-2.0

import unittest


total = 0


def sum1(arg1, arg2):
    total = arg1 + arg2
    return total


def sum2(arg1, arg2):
    global total
    total = arg1 + arg2
    return total


a = 10


# global
def test1():
    global a
    a += 1
    return a


def test2(a):
    a += 1
    return a


# nonlocal
def outer1():
    num = 10

    def inner():
        num = 20

    inner()
    return num


def outer2():
    num = 10

    def inner():
        nonlocal num
        num = 20

    inner()
    return num


class TestNamespace(unittest.TestCase):

    def test_namespace(self):
        self.assertEqual(0, total)
        self.assertEqual(3, sum1(1, 2))
        self.assertEqual(0, total)

        self.assertEqual(0, total)
        self.assertEqual(3, sum2(1, 2))
        self.assertEqual(3, total)

        self.assertEqual(10, a)
        self.assertEqual(11, test1())
        self.assertEqual(11, a)
        self.assertEqual(12, test2(a))
        self.assertEqual(11, a)

        self.assertEqual(10, outer1())
        self.assertEqual(20, outer2())


if __name__ == '__main__':
    unittest.main()
