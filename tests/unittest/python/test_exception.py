#!/usr/bin/env python3
# Copyright (c) 2024 maminjie <canpool@163.com>
# SPDX-License-Identifier: MulanPSL-2.0

import unittest


class MyError(Exception):
    """my exception"""
    def __init__(self, value):
        self.value = value

    def __str__(self):
        return repr(self.value)


class TestException(unittest.TestCase):
    """test exception"""

    def test_builtin_exception(self):
        try:
            '2' + 2
        except TypeError as e:
            self.assertEqual('can only concatenate str (not "int") to str', str(e))

        try:
            x = 1 / 0
        except ZeroDivisionError as e:
            self.assertEqual("division by zero", str(e))

        try:
            x = y + 1
        except NameError as e:
            self.assertEqual("name 'y' is not defined", str(e))

        try:
            x = int("str")
        except ValueError as e:
            self.assertEqual("invalid literal for int() with base 10: 'str'", str(e))

        try:
            f = open("pycanpool.py")
        except OSError as e:
            self.assertEqual("[Errno 2] No such file or directory: 'pycanpool.py'", str(e))

        # self.assertEqual("x", str(e))

    def test_statement(self):
        try:
            1 / 0 + y
        except (ZeroDivisionError, NameError) as e:
            self.assertEqual("division by zero", str(e))

        try:
            y + 1 / 0
        except (ZeroDivisionError, NameError) as e:
            self.assertEqual("name 'y' is not defined", str(e))

        x = 0
        try:
            x /= 0
        except ZeroDivisionError:
            x = 1
            try:
                x = y
            except NameError:
                x = 2
        self.assertEqual(2, x)

        # try-except nest
        x = 0
        try:
            try:
                x = 1 / 0
            except ZeroDivisionError:
                x = 1
            x = y
        except NameError:
            x = 2
        self.assertEqual(2, x)

        # try-except-else
        x = 0
        try:
            x = 1   # 1
        except ZeroDivisionError:
            x += 2
        else:
            x += 3  # 1 + 4
        self.assertEqual(4, x)

        # try-except-else-finally
        x = 0
        try:
            x = 1 / 0
        except ZeroDivisionError:
            x = 2   # 2
        else:
            x = 3
        finally:
            x += 4  # 2 + 4
        self.assertEqual(6, x)

    def test_raise(self):
        try:
            raise Exception("raise my exception")
        except Exception as e:
            self.assertEqual("raise my exception", str(e))

        x = 0
        try:
            try:
                x = 1 / 0
            except ZeroDivisionError:
                x = 2   # 2
                raise
        except ZeroDivisionError:
            x += 3  # 2 + 3
        self.assertEqual(5, x)

        x = 0
        try:
            try:
                x = 1 / 0
            except ZeroDivisionError:
                x = 2  # 2
                raise ValueError
        except ValueError:
            x += 3  # 2 + 3
        self.assertEqual(5, x)

        # try:
        #     1 / 0
        # except ZeroDivisionError:
        #     raise ValueError from None

    def test_my_exception(self):
        x = 0
        try:
            raise MyError(2 * 2)
        except MyError as e:
            x = e.value
        self.assertEqual(4, x)

    def test_exception_group(self):
        try:
            excs = [OSError('error 1'), SystemError('error 2')]
            raise ExceptionGroup('there were problems', excs)
        except Exception as e:
            self.assertEqual("caught <class 'ExceptionGroup'>: there were problems (2 sub-exceptions)",
                             f'caught {type(e)}: {e}')

        try:
            raise ExceptionGroup(
                "group1",
                [
                    OSError(1),
                    SystemError(2),
                    ExceptionGroup(
                        "group2",
                        [
                            OSError(3),
                            RecursionError(4)
                        ]
                    )
                ]
            )
        except* OSError as e:
            self.assertEqual("group1 (2 sub-exceptions)", str(e))
        except* SystemError as e:
            self.assertEqual("group1 (1 sub-exception)", str(e))
        except* RecursionError as e:
            self.assertEqual("group1 (1 sub-exception)", str(e))

        excs = []
        try:
            try:
                1 / 0
            except ZeroDivisionError as e:
                excs.append(e)
                raise ValueError
        except ValueError as e:
            excs.append(e)

        try:
            if excs:
                raise ExceptionGroup("Test Failures", excs)
        except Exception as e:
            self.assertEqual("('Test Failures', [ZeroDivisionError('division by zero'), ValueError()])",
                             str(e.args))

    def test_add_note(self):
        s = ""
        try:
            1 / 0
        except ZeroDivisionError as e:
            e.add_note("add some note")
            s += str(e)
        self.assertEqual("division by zero", s)


if __name__ == '__main__':
    unittest.main()
