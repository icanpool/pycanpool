#!/usr/bin/env python3
# Copyright (c) 2024 maminjie <canpool@163.com>
# SPDX-License-Identifier: MulanPSL-2.0

import unittest


class TestMisc(unittest.TestCase):

    def test_condition_control(self):
        # chain operation
        self.assertTrue(1 < 2 == 2)     # 1 < 2 and 2 == 2
        self.assertFalse(1 < 2 == 1)    # 1 < 2 and 2 == 1

        # short-circuit operation
        s1, s2, s3 = '', 'a', 'b'
        # The first non-null string
        r = s1 or s2 or s3              # result is s2
        self.assertEqual('a', r)
        # The first null string or the last non-null string
        r = s1 and s2 and s3            # result is s1
        self.assertEqual('', r)
        r = s2 and s3                   # result is s3
        self.assertEqual('b', s3)

    def test_zip(self):
        names = ['anne', 'beth', 'george', 'damon']
        ages = [12, 45, 32, 102]

        n1, a1 = names[0], ages[0]
        tup = list(zip(names, ages))[0]
        self.assertEqual(('anne', 12), tup)
        n2, a2 = tup
        self.assertEqual(n1, n2)
        self.assertEqual(a1, a2)

        s1 = ""
        for i in range(len(names)):
            s1 += names[i] + str(ages[i])
        s2 = ""
        for name, age in zip(names, ages):
            s2 += name + str(age)
        self.assertEqual(s1, s2)

        # depending on the shortest sequence
        lst = list(zip(range(5), range(100000000)))
        self.assertEqual([(0, 0), (1, 1), (2, 2), (3, 3), (4, 4)], lst)

    def test_enumerate(self):
        ss = ['yx', 'xx', 'zx']
        for s in ss:
            if 'x' in s:
                i = ss.index(s)
                ss[i] = 'en'
        self.assertEqual(['en', 'en', 'en'], ss)

        ss = ['yx', 'xx', 'zx']
        i = 0
        for s in ss:
            if 'x' in s:
                ss[i] = 'en'
            i += 1
        self.assertEqual(['en', 'en', 'en'], ss)

        ss = ['yx', 'xx', 'zx']
        for i, s in enumerate(ss):
            if 'x' in s:
                ss[i] = 'en'
        self.assertEqual(['en', 'en', 'en'], ss)


if __name__ == '__main__':
    unittest.main()
