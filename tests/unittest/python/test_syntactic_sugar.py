#!/usr/bin/env python3
# Copyright (c) 2024 maminjie <canpool@163.com>
# SPDX-License-Identifier: MulanPSL-2.0

import unittest
from functools import wraps


class MyNumbers:
    def __iter__(self):
        self.a = 1
        return self

    def __next__(self):
        if self.a <= 5:
            x = self.a
            self.a += 1
            return x
        else:
            raise StopIteration


def fibonacci_generator(n):
    """generator"""
    a, b, counter = 0, 1, 0
    while True:
        if counter > n:
            return
        yield a
        a, b = b, a + b
        counter += 1


def repeater_generator(value):
    """
    yield returns a value -- the value sent from the outside world via send.
    If next is used, the yield returns None.
    """
    while True:
        new = (yield value)
        if new is not None:
            value = new


def a_new_decorator(a_func):
    def wrap_function():
        s = "wrap "
        s += a_func()
        return s

    return wrap_function


def a_new_decorator_2(a_func):
    @wraps(a_func)
    def wrap_function():
        s = "wrap "
        s += a_func()
        return s

    return wrap_function


def a_function_requiring_decoration_1():
    return "f"


@a_new_decorator
def a_function_requiring_decoration_2():
    return "f"


@a_new_decorator_2
def a_function_requiring_decoration_3():
    return "f"


def f_decorator(f):
    @wraps(f)
    def decorated(*args, **kwargs):
        s = "wrap "
        s += f(*args, **kwargs)
        return s
    return decorated


@f_decorator
def f_say_1(msg):
    return f"say {msg}"


def wrap_decorator(wrap="wrap"):
    def f_decorator(f):
        @wraps(f)
        def decorated(*args, **kwargs):
            s = f"{wrap} "
            s += f(*args, **kwargs)
            return s

        return decorated

    return f_decorator


@wrap_decorator()
def f_say_2(msg):
    return f"say {msg}"


@wrap_decorator('ww')
def f_say_3(msg):
    return f"say {msg}"


@f_decorator
@wrap_decorator('ww')
def f_say_4(msg):
    return f"say {msg}"


class MyDecorator(object):
    def __init__(self, wrap="wrap"):
        self.wrap = wrap

    def __call__(self, f):
        @wraps(f)
        def decorated(*args, **kwargs):
            s = f"{self.wrap} "
            s += f(*args, **kwargs)
            return s

        return decorated


@MyDecorator()
def f_say_5(msg):
    return f"say {msg}"


@MyDecorator('class')
def f_say_6(msg):
    return f"say {msg}"


class TestSyntacticSugar(unittest.TestCase):

    def test_sequence_unpack(self):
        x, y = 1, 2
        self.assertEqual(1, x)
        self.assertEqual(2, y)
        x, y = y, x
        self.assertEqual(2, x)
        self.assertEqual(1, y)
        values = 3, 4
        self.assertEqual((3, 4), values)
        x, y = values
        self.assertEqual(3, x)
        self.assertEqual(4, y)

        try:
            x, y, z = 1, 2
        except ValueError as e:
            self.assertEqual("not enough values to unpack (expected 3, got 2)", str(e))
        try:
            x, y, z = 1, 2, 3, 4
        except ValueError as e:
            self.assertEqual("too many values to unpack (expected 3)", str(e))

        # rest is always list
        x, y, *rest = 1, 2
        self.assertEqual([], rest)
        x, y, *rest = 1, 2, 3
        self.assertEqual([3], rest)
        x, y, *rest = 1, 2, 3, 4
        self.assertEqual([3, 4], rest)
        x, *rest, y = 1, 2
        self.assertEqual(2, y)
        self.assertEqual([], rest)
        x, *rest, y = 1, 2, 3
        self.assertEqual(3, y)
        self.assertEqual([2], rest)
        x, *rest, y = 1, 2, 3, 4
        self.assertEqual(4, y)
        self.assertEqual([2, 3], rest)
        a, *b, c = "abc"
        self.assertEqual('a', a)
        self.assertEqual(['b'], b)
        self.assertEqual('c', c)

    def test_comprehension(self):
        # list
        lst = [1, 2, 3]
        self.assertEqual([1, 4, 9], [i ** 2 for i in lst])

        names = ['Bob','Tom','alice','Jerry','Wendy','Smith']
        new_names = [name.upper() for name in names if len(name) > 3]
        self.assertEqual(['ALICE', 'JERRY', 'WENDY', 'SMITH'], new_names)

        multiples = [i for i in range(30) if i % 3 == 0]
        self.assertEqual([0, 3, 6, 9, 12, 15, 18, 21, 24, 27], multiples)

        matrix = [
            [1, 2, 3, 4],
            [5, 6, 7, 8],
            [9, 10, 11, 12],
        ]
        new_matrix = [[row[i] for row in matrix] for i in range(4)]
        self.assertEqual([[1, 5, 9], [2, 6, 10], [3, 7, 11], [4, 8, 12]], new_matrix)
        self.assertEqual([(1, 5, 9), (2, 6, 10), (3, 7, 11), (4, 8, 12)], list(zip(*matrix)))

        # dict
        lst = ['Google', 'Baidu', 'Bing']
        dic = {key: len(key) for key in lst}
        self.assertEqual({'Google': 6, 'Baidu': 5, 'Bing': 4}, dic)
        dic = {key: len(key) for key in lst if 'i' in key}
        self.assertEqual({'Baidu': 5, 'Bing': 4}, dic)

        dic = {x: x**2 for x in (2, 4, 6)}
        self.assertEqual({2: 4, 4: 16, 6: 36}, dic)

        # set
        s = {i**2 for i in (1, 2, 3, 2)}
        self.assertEqual({1, 4, 9}, s)
        s = {x for x in 'abracadabra' if x not in 'abc'}
        self.assertEqual({'d', 'r'}, s)

        # tuple
        a = (x for x in range(1, 5))       # a is generator object
        self.assertEqual("<class 'generator'>", str(type(a)))
        self.assertEqual((1, 2, 3, 4), tuple(a))

    def test_iterator(self):
        lst = [1, 2]
        it = iter(lst)
        self.assertEqual(1, next(it))
        self.assertEqual(2, next(it))
        try:
            next(it)
        except StopIteration:
            pass

        it = iter(lst)
        sum = 0
        for x in it:
            sum += x
        self.assertEqual(3, sum)

        it = iter(lst)
        sum = 0
        while True:
            try:
                sum += next(it)
            except StopIteration:
                break
        self.assertEqual(3, sum)

        it = iter(MyNumbers())
        sum = 0
        while True:
            try:
                sum += next(it)
            except StopIteration:
                break
        self.assertEqual(15, sum)

    def test_generator(self):
        f = fibonacci_generator(10)   # f is iterator
        x = 0
        while True:
            try:
                x = next(f)     # 0 1 1 2 3 5 8 13 21 34 55
            except StopIteration:
                break
        self.assertEqual(55, x)

        r = repeater_generator(42)
        self.assertEqual(42, next(r))
        self.assertEqual(42, next(r))
        r.send('hi')
        self.assertEqual('hi', next(r))
        self.assertEqual('hi', next(r))

    def test_decorator(self):
        f = a_function_requiring_decoration_1
        self.assertEqual("f", f())
        w_f = a_new_decorator(f)
        self.assertEqual("wrap f", w_f())
        self.assertEqual("a_function_requiring_decoration_1", f.__name__)
        self.assertEqual("wrap_function", w_f.__name__)

        f = a_function_requiring_decoration_2
        self.assertEqual("wrap f", f())
        self.assertEqual("wrap_function", f.__name__)

        f = a_function_requiring_decoration_3
        self.assertEqual("wrap f", f())
        self.assertEqual("a_function_requiring_decoration_3", f.__name__)

        f = f_say_1
        self.assertEqual("wrap say hi", f('hi'))
        self.assertEqual("wrap say bye", f('bye'))

        f = f_say_2
        self.assertEqual("wrap say hi", f('hi'))

        f = f_say_3
        self.assertEqual("ww say hi", f('hi'))

        f = f_say_4
        self.assertEqual("wrap ww say hi", f('hi'))

        f = f_say_5
        self.assertEqual("wrap say hi", f('hi'))

        f = f_say_6
        self.assertEqual("class say hi", f('hi'))


if __name__ == '__main__':
    unittest.main()
