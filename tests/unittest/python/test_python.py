#!/usr/bin/env python3
# Copyright (c) 2024 maminjie <canpool@163.com>
# SPDX-License-Identifier: MulanPSL-2.0

"""
reference:
    https://docs.python.org/zh-cn/3/library/unittest.html

usage:
    python3 tests/test_something.py

    python3 -m unittest test_module1 test_module2
    python3 -m unittest test_module.TestClass
    python3 -m unittest test_module.TestClass.test_method

    python -m unittest tests/test_something.py

    python -m unittest -h   # for more details
"""

import unittest


class TestPython(unittest.TestCase):
    """Test the builtin elements of python"""
    def setUp(self):
        pass

    def tearDown(self):
        pass

    def test_print(self):
        print("Hello World")
        print("hello Python\n", end="")

    def test_variable(self):
        x = 1
        self.assertEqual(1, x)
        a = b = 1
        self.assertEqual(a, b)
        self.assertEqual(id(a), id(b))
        self.assertTrue(a is b)
        a, b = 2, 3
        self.assertEqual(2, a)
        self.assertEqual(3, b)
        self.assertFalse(a is b)
        a, b = 4, 4
        self.assertEqual(a, b)
        self.assertTrue(a is b)

    def test_string(self):
        # convert
        self.assertEqual('abc'.upper(), 'ABC')
        self.assertTrue('A1'.isupper())

        self.assertEqual("ABC".lower(), 'abc')
        self.assertTrue("abc123".islower())
        self.assertFalse("123".islower())

        self.assertEqual('abc'.title(), 'Abc')
        self.assertEqual('aBc'.title(), 'Abc')
        self.assertFalse("ABc".istitle())

        # join
        s = "a" + "b"
        self.assertEqual("ab", s)
        s = "a" * 3
        self.assertEqual("aaa", s)
        s = "a" "b"
        self.assertEqual("ab", s)
        s = "a" \
            "b"
        self.assertEqual("ab", s)
        s = "a \
b"
        self.assertEqual("a b", s)
        s = """a 
b"""
        self.assertEqual("a \nb", s)
        s = r"line \n"      # raw string
        self.assertEqual("line \\n", s)

        self.assertEqual("a.b.c", ".".join("abc"))

        # strip
        self.assertEqual(" a b ".rstrip(), " a b")
        self.assertEqual(" a b ".lstrip(), "a b ")
        self.assertEqual(" a b ".strip(), "a b")

        # in or not in
        self.assertTrue("o" in "hello")
        self.assertTrue("i" not in "hello")

        # slice
        s = "pycanpool"
        self.assertEqual("pycanpool",   s)
        self.assertEqual("p",           s[0])
        self.assertEqual("pycanpoo",    s[0:-1])
        self.assertEqual("can",         s[2:5])
        self.assertEqual("canpool",     s[2:])
        self.assertEqual("l",           s[-1])
        self.assertEqual("l",           s[-1:])
        self.assertEqual("o",           s[-2])
        self.assertEqual("ol",          s[-2:])
        self.assertEqual("o",           s[-2:-1])
        self.assertEqual("oo",          s[-3:8])
        self.assertEqual("cn",          s[2:6:2])
        self.assertEqual("cpl",         s[2::3])

        # format
        s = "%s %d" % ('ab', 10)        # old-style
        self.assertEqual("ab 10", s)
        s = "{} {}".format('ab', 10)
        self.assertEqual("ab 10", s)

        # str.format from python 2.6
        self.assertEqual("a b",     "{0} {1}".format('a', 'b'))
        self.assertEqual("a {1}",   "{0} {{1}}".format('a'))
        self.assertEqual("b a",     "{1} {0}".format('a', 'b'))
        self.assertEqual("a a b",   "{0} {0} {1}".format('a', 'b'))
        self.assertEqual("k 10",    "{a1} {a2}".format(a1="k", a2=10))
        self.assertEqual("k 10",    "{a1} {a2}".format(a2=10, a1="k"))
        dic = {"a1": "k", "a2": 10}
        self.assertEqual("k 10",    "{a1} {a2}".format(**dic))
        lst = ['k', 10]
        self.assertEqual("k 10",    "{0[0]} {0[1]}".format(lst))

        # f-string from python 3.6
        s = "b"
        self.assertEqual("ab",      f'a{s}')
        self.assertEqual('3',       f'{1 + 2}')
        self.assertEqual("k 10",    f'{dic["a1"]} {dic["a2"]}')
        x = 1
        self.assertEqual('2',       f'{x+1}')       # python 3.6
        self.assertEqual('x+1=2',   f'{x+1=}')      # python 3.8

        # number format
        self.assertEqual("3.14",    "{:.2f}".format(3.1415926))
        self.assertEqual("+3.14",   "{:+.2f}".format(3.1415926))
        self.assertEqual("-1.00",   "{:-.2f}".format(-1))
        self.assertEqual("3",       "{:.0f}".format(2.71828))
        self.assertEqual("05",      "{:0>2d}".format(5))
        self.assertEqual("5xxx",    "{:x<4d}".format(5))
        self.assertEqual("10xx",    "{:x<4d}".format(10))
        self.assertEqual("1,000,000",   "{:,}".format(1000000))
        self.assertEqual("25.00%",      "{:.2%}".format(0.25))
        self.assertEqual("1.00e+09",    "{:.2e}".format(1000000000))
        self.assertEqual("        13",  "{:>10d}".format(13))   # right alignment
        self.assertEqual("13        ",  "{:<10d}".format(13))   # left alignment
        self.assertEqual("    13    ",  "{:^10d}".format(13))   # middle alignment
        self.assertEqual("1011",    "{:b}".format(11))
        self.assertEqual("11",      "{:d}".format(11))
        self.assertEqual("13",      "{:o}".format(11))
        self.assertEqual("b",       "{:x}".format(11))
        self.assertEqual("0xb",     "{:#x}".format(11))
        self.assertEqual("0XB",     "{:#X}".format(11))

    def test_number(self):
        # integer
        self.assertEqual(5, 2 + 3)
        self.assertEqual(1, 3 - 2)
        self.assertEqual(6, 2 * 3)
        self.assertEqual(1.5, 3 / 2)
        self.assertEqual(1, 3 // 2)
        self.assertEqual(2, 5 % 3)
        self.assertEqual(9, 3 ** 2)
        # float
        self.assertEqual(0.2, 0.1 + 0.1)
        self.assertEqual(0.4, 0.2 + 0.2)
        self.assertEqual(0.2, 2 * 0.1)
        self.assertEqual(0.4, 2 * 0.2)
        self.assertNotEqual(0.3, 0.2 + 0.1)  # real is 0.30000000000000004
        self.assertNotEqual(0.3, 3 * 0.1)  # real is 0.30000000000000004
        # bit
        a, b = 60, 13
        self.assertEqual(12, a & b)
        self.assertEqual(61, a | b)
        self.assertEqual(49, a ^ b)
        self.assertEqual(-61, ~a)
        self.assertEqual(240, a << 2)
        self.assertEqual(15, a >> 2)

        self.assertEqual(123, int("123"))

    def test_type(self):
        self.assertEqual("23", str(23))

        a, b, c, d = 20, 5.5, True, 4 + 3j
        self.assertEqual("<class 'int'>", str(type(a)))
        self.assertNotEqual(int(), type(a))
        self.assertEqual("<class 'float'>", str(type(b)))
        self.assertEqual("<class 'bool'>", str(type(c)))
        self.assertEqual("<class 'complex'>", str(type(d)))
        self.assertTrue(isinstance(a, int))
        self.assertTrue(isinstance(b, float))
        self.assertTrue(isinstance(c, bool))
        self.assertTrue(isinstance(d, complex))
        self.assertTrue(isinstance('c', str))

        self.assertTrue(issubclass(bool, int))
        self.assertEqual(1, True)
        self.assertEqual(0, False)
        self.assertEqual(2, True + 1)

    def test_list(self):
        lst = [123, 1.0, "abc", "a c"]
        # get
        self.assertEqual(123, lst[0])
        self.assertEqual("a c", lst[-1])
        self.assertEqual("abc", lst[-2])
        # modify
        lst[0] = 321  # [321, 1.0, "abc", "a c"]
        self.assertEqual(321, lst[0])
        # add
        lst.append("py")  # [321, 1.0, "abc", "a c", "py"]
        self.assertEqual("py", lst[-1])
        lst.insert(0, 12)  # [12, 321, 1.0, "abc", "a c", "py"]
        self.assertEqual(12, lst[0])
        # remove
        del lst[0]  # [321, 1.0, "abc", "a c", "py"]
        self.assertNotIn(12, lst)
        self.assertEqual(321, lst[0])
        x = lst.pop()  # [321, 1.0, "abc", "a c"]
        self.assertEqual("py", x)
        self.assertNotIn(x, lst)
        x = lst.pop(0)  # [1.0, "abc", "a c"]
        self.assertEqual(321, x)
        self.assertEqual(1.0, lst[0])
        lst.remove(1.0)  # [ "abc", "a c"]
        self.assertEqual("abc", lst[0])
        x = "abc"
        lst.remove(x)  # ["a c"]
        self.assertNotIn(x, lst)
        # Only the first value is removed
        lst.insert(0, 12)  # [12, "a c"]
        lst.append(12)  # [12, "a c", 12]
        lst.remove(12)  # ["a c", 12]
        self.assertIn(12, lst)
        self.assertEqual("a c", lst[0])

        lst = ["cba", "abc", "bca", "bac"]
        self.assertEqual("cba", lst[0])
        lst.sort()  # ['abc', 'bac', 'bca', 'cba']
        self.assertEqual("abc", lst[0])
        lst.sort(reverse=True)  # ['cba', 'bca', 'bac', 'abc']
        self.assertEqual("abc", lst[-1])
        x = sorted(lst)  # temporary
        self.assertEqual("abc", x[0])
        self.assertEqual("abc", lst[-1])
        lst.reverse()  # ['abc', 'bac', 'bca', 'cba']
        self.assertEqual("abc", lst[0])
        self.assertEqual(4, len(lst))
        # print(lst[4])     # IndexError: list index out of range

        empty_lst = []
        self.assertEqual(0, len(empty_lst))
        self.assertFalse(empty_lst)

        # slice
        lst = [123, 1.0, "abc", "a c"]
        self.assertEqual([123, 1.0], lst[0:2])  # [0, 2)
        self.assertEqual(lst[:2], lst[0:2])
        self.assertEqual(["abc", "a c"], lst[2:4])
        self.assertEqual(lst[2:], lst[2:4])
        self.assertEqual(lst[2:], lst[-2:])
        self.assertEqual(["abc"], lst[-2:-1])
        self.assertEqual(lst, lst[:])

        # copy
        lst = [1, 2, 3]
        lst1 = lst[:]
        lst2 = lst
        lst1.append(4)
        lst2.append(5)
        self.assertEqual(lst, lst2)
        self.assertNotEqual(lst, lst1)

    def test_range(self):
        x = 0
        for i in range(1, 5):  # [1, 5)
            x += i
        self.assertEqual(10, x)

        x = 0
        for i in range(5):  # [0, 5)
            x += i
        self.assertEqual(10, x)

        x = 0
        for i in range(2, 11, 2):  # 2,4.6.8.10
            x += i
        self.assertEqual(30, x)

        lst = list(range(1, 5))
        self.assertEqual(1, min(lst))
        self.assertEqual(4, max(lst))
        self.assertEqual(10, sum(lst))

    def test_tuple(self):
        tup = (1, 2)
        self.assertEqual(1, tup[0])
        # tup[0] = 11   # TypeError: 'tuple' object does not support item assignment

        empty_tup = ()
        self.assertEqual(0, len(empty_tup))
        self.assertFalse(empty_tup)

        one_tup = (1,)
        self.assertEqual(1, len(one_tup))
        self.assertTrue(one_tup)

    def test_condition(self):
        s = "abc"
        self.assertFalse(s == "Abc")
        self.assertTrue(s != "Abc")
        x = 10
        self.assertTrue(x == 10)
        self.assertTrue(x < 20)
        self.assertTrue(x <= 20)
        self.assertFalse(x > 20)
        self.assertFalse(x >= 20)
        y = 21
        self.assertFalse(x >= 20 and y >= 20)
        x = 20
        self.assertTrue(x >= 20 and y >= 20)  # (x >= 20) and (y >= 20)
        self.assertTrue(x >= 21 or y >= 21)
        y = 20
        self.assertFalse(x >= 21 or y >= 21)

        lst = [1, 2, 3]
        self.assertTrue(1 in lst)
        self.assertFalse(4 in lst)
        self.assertFalse(2 not in lst)

    def test_if(self):
        x = 5
        if x >= 5:
            x = 1  # here
        self.assertTrue(x == 1)

        # if-else
        if x >= 5:
            x = 2
        else:
            x = 3  # here
        self.assertEqual(3, x)

        # if-elif-else
        if x < 2:
            x = 10
        elif x < 10:
            x = 5  # here
        else:
            x = 3
        self.assertEqual(5, x)

        # if-elif-elif-...-else
        if x < 2:
            x = 10
        elif x < 10:
            x = 10  # here
        elif x < 20:
            x = 5
        else:
            x = 3
        self.assertEqual(10, x)

        # if-elif-elif-...-elif
        if x < 2:
            x = 10
        elif x < 10:
            x = 10
        elif x < 20:
            x = 5  # here
        elif x < 6:
            x = 3
        self.assertEqual(5, x)

    def test_dict(self):
        dic = {"a": 1, "b": "B"}
        self.assertEqual(1, dic["a"])
        dic["x"] = 2
        dic["y"] = 3
        self.assertEqual(2, dic["x"])
        dic["a"] = 4
        self.assertEqual(4, dic["a"])
        del dic["x"]
        # print(dic["x"])   # KeyError: 'x'

        for k, v in dic.items():
            print(k, v)
        # for k in dic.keys():
        #     print(k)
        # for k in dic:
        #     print(k)
        # for v in dic.values():
        #     print(v)
        x = ""
        for k in sorted(dic.keys()):
            x += k
        else:
            self.assertEqual("aby", x)
        self.assertEqual("aby", x)

        empty_dic = ()
        self.assertEqual(0, len(empty_dic))
        self.assertFalse(empty_dic)

    def test_set(self):
        lst = [2, 1, 3, 2]
        s = set(lst)
        self.assertEqual({1, 2, 3}, s)

        empty_set = set()
        self.assertEqual(0, len(empty_set))
        self.assertFalse(empty_set)

    def test_while(self):
        x = 0
        i = 1
        while i <= 5:
            x += i
            i += 1
        self.assertEqual(15, x)

        x = 0
        i = 1
        while i <= 5:
            if i % 2 == 0:
                i += 1
                continue
            x += i  # 0 + 1 + 3
            if i == 3:
                break
            i += 1
        else:
            self.assertEqual(3, i)
        self.assertEqual(4, x)

        lst = [1, 2, 3, 2]
        while 2 in lst:
            lst.remove(2)
        self.assertEqual([1, 3], lst)


if __name__ == '__main__':
    unittest.main()
