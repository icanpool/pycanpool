#!/usr/bin/env python3
# Copyright (c) 2024 maminjie <canpool@163.com>
# SPDX-License-Identifier: MulanPSL-2.0

"""type and variable annotation"""

import unittest


def f_max(a: int, b: int) -> int:
    return a if a > b else b


def f_people(name: str, age: 'int > 0' = 18) -> tuple:
    return name, age


def f_stringify(arg: any) -> str:
    return str(arg)


class TestAnnotation(unittest.TestCase):
    def test_annotation(self):
        self.assertEqual(3, f_max(1, 3))
        self.assertEqual(('zhang san', 18), f_people('zhang san'))
        self.assertEqual(('li si', 20), f_people('li si', 20))

        self.assertEqual('20', f_stringify(20))
        self.assertEqual('(1,)', f_stringify((1,)))
        self.assertEqual("{'name': 'wang wu'}", f_stringify({'name': 'wang wu'}))

        self.assertEqual("int > 0", f_stringify(f_people.__annotations__['age']))

        names: list = ['python', 'qt']
        version: tuple = (3, 12, 1)
        # major, minor, patch = version
        self.assertEqual(['python', 'qt'], names)
        self.assertEqual((3, 12, 1), version)


if __name__ == '__main__':
    unittest.main()
