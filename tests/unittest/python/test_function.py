#!/usr/bin/env python3
# Copyright (c) 2024 maminjie <canpool@163.com>
# SPDX-License-Identifier: MulanPSL-2.0

import unittest


def f_print():
    print("Hello World")


def f_print_msg(msg):
    print(msg)


def f_multi_arg(param1, param2):
    return param2 + param1


def f_default_arg(param1, param2='d'):
    return param2 + param1


def f_default_arg_mutable(a, L=[]):
    """The default value is calculated only once"""
    L.append(a)
    return L


def f_default_arg_none(a, L=None):
    if L is None:
        L = []
    L.append(a)
    return L


def f_return_dict(param1, param2):
    res = {'p1': param1, 'p2': param2}
    return res


def f_change_immutable(a):
    a = 10
    return a


def f_nochange_mutable(lst):
    x = ""
    for i in lst:
        x += i
    return x


def f_change_mutable(lst):
    x = ""
    for i in lst:
        x += i
    lst.append("x")
    return x


def f_tuple_args(*args):
    return args


def f_dict_args(**kwargs):
    return kwargs


def f_mandatory_kwarg(a, b, *, kwd):
    return a + b + kwd


def f_special_args(pos1, pos2, /, pos_or_kwd1, pos_or_kwd2, *, kwd1, kwd2):
    return pos1 + pos2 + pos_or_kwd1 + pos_or_kwd2 + kwd1 + kwd2


def f_return_lambda(n):
    return lambda a: a * n


class TestFunction(unittest.TestCase):
    def test_function(self):
        f_print()
        f_print_msg("Hello Function")

        # position arguments
        self.assertEqual(3, f_multi_arg(1, 2))
        self.assertEqual(3, f_multi_arg(2, 1))
        self.assertEqual("ba", f_multi_arg("a", "b"))
        # keyword arguments
        self.assertEqual("ab", f_multi_arg(param2="a", param1="b"))
        # default value
        self.assertEqual("da", f_default_arg("a"))
        self.assertEqual("ba", f_default_arg("a", "b"))
        self.assertEqual("ab", f_default_arg(param2="a", param1="b"))

        self.assertEqual([1], f_default_arg_mutable(1))
        self.assertEqual([1, 2], f_default_arg_mutable(2))
        self.assertEqual([1, 2, 3], f_default_arg_mutable(3))
        self.assertEqual([1], f_default_arg_none(1))
        self.assertEqual([2], f_default_arg_none(2))
        self.assertEqual([3], f_default_arg_none(3))

        # returning a dictionary
        self.assertEqual({"p1": 1, "p2": 2}, f_return_dict(1, 2))

        a = 1
        self.assertEqual(10, f_change_immutable(a))
        self.assertEqual(1, a)
        a = 3
        self.assertEqual(10, f_change_immutable(a))
        self.assertEqual(3, a)

        # passing a list
        lst = ["a", "b", "c"]
        self.assertEqual("abc", f_nochange_mutable(lst))
        self.assertEqual(["a", "b", "c"], lst)
        # modifying a list in a function
        self.assertEqual("abc", f_change_mutable(lst))
        self.assertEqual(["a", "b", "c", "x"], lst)
        # preventing a function from modifying a list
        lst = ["c", "b", "a"]
        self.assertEqual("cba", f_change_mutable(lst[:]))
        self.assertEqual(["c", "b", "a"], lst)

        self.assertEqual((), f_tuple_args())
        self.assertEqual((1,), f_tuple_args(1))
        self.assertEqual((1, 2), f_tuple_args(1, 2))
        self.assertEqual((1, "a"), f_tuple_args(1, "a"))
        # unpacking
        tup = (1,)
        self.assertEqual((1,), f_tuple_args(*tup))
        tup = (1, 2)
        self.assertEqual((1, 2), f_tuple_args(*tup))
        self.assertEqual((1, "a"), f_tuple_args(*(1, "a")))

        self.assertEqual({"a": 1, "b": 2}, f_dict_args(a=1, b=2))
        # unpacking
        dic = {"a": 1}
        self.assertEqual({"a": 1}, f_dict_args(**dic))
        self.assertEqual({"a": 1, "b": 2}, f_dict_args(**{"a": 1, "b": 2}))

        self.assertEqual(4, f_mandatory_kwarg(1, 1, kwd=2))

        self.assertEqual("abcdef",
                         f_special_args("a", "b", "c", pos_or_kwd2="d", kwd1="e", kwd2="f"))
        self.assertEqual("abcdef",
                         f_special_args("a", "b", "c", "d", kwd2="f", kwd1="e"))
        self.assertEqual("abcdef",
                         f_special_args("a", "b", pos_or_kwd2="d", pos_or_kwd1="c", kwd1="e", kwd2="f"))

        f_sum = lambda arg1, arg2: arg1 + arg2
        self.assertEqual(10, f_sum(1, 9))
        self.assertEqual(10, f_sum(6, 4))

        f_2 = f_return_lambda(2)
        f_3 = f_return_lambda(3)
        self.assertEqual(10, f_2(5))
        self.assertEqual(15, f_3(5))


if __name__ == '__main__':
    unittest.main()
