#!/usr/bin/env python3
# Copyright (c) 2024 maminjie <canpool@163.com>
# SPDX-License-Identifier: MulanPSL-2.0

import datetime
import unittest


class Language(object):
    """base class"""

    def __init__(self, name, year):
        """constructor method"""
        self.name = name
        self.year = year
        self.rank = 1   # TIOBE index

    def get_age(self):
        return datetime.date.today().year - self.year

    def get_rank(self):
        return self.rank

    def set_rank(self, index):
        self.rank = index

    index = property(get_rank, set_rank)


class CppLanguage(Language):
    """c++ language"""

    def __init__(self):
        super().__init__("C++", 1979)


class MyLanguage(Language):
    """my custom language"""

    def __init__(self, name, year, level="high"):
        super().__init__(name, year)
        # add new attribute
        self.level = level

    def set_rank(self, index):
        """rewrite parent method"""
        if index > 3 or index < 1:
            self.rank = 3
        else:
            self.rank = index


class MyClass(object):
    __private_attr = "pri"
    public_attr = "pub"     # class attr

    def __init__(self, pub="ins_pub", pri="ins_pri"):
        self.ins_pub_attr = pub
        self.__ins_pri_attr = pri
        if pub != "ins_pub":
            self.public_attr = pub  # instance attr

    def __pri_method(self):
        return self.__ins_pri_attr

    def pub_method(self):
        return self.__pri_method()

    @staticmethod
    def sta_method():
        return 1 + 2

    @classmethod
    def cls_method(cls):
        return cls.public_attr + cls.__private_attr

    @classmethod
    def cls_get_pub_attr(cls):
        return cls.public_attr

    @classmethod
    def cls_set_pub_attr(cls, pub):
        cls.public_attr = pub


class MyVector(object):
    """my vector"""

    def __init__(self, a, b):
        self.a = a
        self.b = b

    def __str__(self):
        return f"Vector ({self.a}, {self.b})"

    def __call__(self, a, b):
        return f"Vector ({a}, {b})"

    def __len__(self):
        return round(((self.a) ** 2 + (self.b) ** 2) ** 0.5)

    def __add__(self, other):
        return MyVector(self.a + other.a, self.b + other.b)


class MyRectangle(object):
    def __init__(self):
        self.width = 0
        self.height = 0

    @property
    def size(self):
        return self.width, self.height

    @size.setter
    def size(self, size):
        self.width, self.height = size


class TestClass(unittest.TestCase):
    """test class"""

    def test_language(self):
        py_lang = Language("Python", 1990)
        self.assertEqual("Python", py_lang.name)
        self.assertEqual(1990, py_lang.year)
        self.assertTrue(py_lang.get_age() >= 34)
        self.assertEqual(1, py_lang.rank)

        c_lang = Language("C", 1972)
        c_lang.rank = 2
        self.assertEqual(2, c_lang.get_rank())
        self.assertEqual(2, c_lang.index)
        c_lang.set_rank(1)
        self.assertEqual(1, c_lang.get_rank())
        c_lang.index = 3
        self.assertEqual(3, c_lang.get_rank())

        cpp_lang = CppLanguage()
        self.assertEqual("C++", cpp_lang.name)
        cpp_lang.set_rank(3)
        self.assertEqual(3, cpp_lang.get_rank())

        my_lang = MyLanguage("D", 2024)
        self.assertEqual("D", my_lang.name)
        my_lang.set_rank(10)
        self.assertEqual(3, my_lang.get_rank())

    def test_class(self):
        self.assertEqual("pub", MyClass.public_attr)
        mc = MyClass()
        self.assertEqual("pub", mc.public_attr)
        # self.assertEqual("pri", mc.__private_attr)    # AttributeError
        self.assertEqual("pub", MyClass.public_attr)
        mc.public_attr = "ins"      # instance attr
        self.assertEqual("ins", mc.public_attr)
        self.assertEqual("pub", MyClass.public_attr)

        self.assertEqual("ins_pub", mc.ins_pub_attr)
        self.assertEqual("ins_pri", mc.pub_method())

        self.assertEqual("pub", MyClass.public_attr)
        mc = MyClass("public")
        self.assertEqual("public", mc.public_attr)
        self.assertEqual("pub", MyClass.public_attr)

        self.assertEqual(3, MyClass.sta_method())
        self.assertEqual(3, mc.sta_method())
        self.assertEqual("pubpri", MyClass.cls_method())
        self.assertEqual("pubpri", mc.cls_method())

        MyClass.public_attr = "cls_pub"
        self.assertEqual("cls_pubpri", mc.cls_method())
        mc.cls_set_pub_attr("public")
        self.assertEqual("public", MyClass.cls_get_pub_attr())

    def test_vector(self):
        v1 = MyVector(2, 10)
        v2 = MyVector(5, -2)
        self.assertEqual('Vector (2, 10)', str(v1))         # __str__
        self.assertEqual('Vector (7, 8)', str(v1 + v2))     # __add__
        self.assertEqual('Vector (3, 2)', v2(3, 2))         # __call__
        v3 = MyVector(-3, 4)
        self.assertEqual(5, len(v3))                        # __len__

        self.assertEqual('my vector', MyVector.__doc__)

    def test_rectangle(self):
        r = MyRectangle()
        self.assertEqual((0, 0), r.size)
        r.width = 3
        r.height = 4
        self.assertEqual((3, 4), r.size)
        r.size = 6, 8
        self.assertEqual(8, r.height)


if __name__ == '__main__':
    unittest.main()
