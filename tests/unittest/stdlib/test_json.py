#!/usr/bin/env python3
# Copyright (c) 2024 maminjie <canpool@163.com>
# SPDX-License-Identifier: MulanPSL-2.0

import unittest
import json


class TestJson(unittest.TestCase):
    """
    string: dumps, loads
    file: dump, load
    """
    def test_dumps(self):
        d1 = [{'a': 1, 'b': 2}, {'c': 3, 'd': 4, 'e': 5}]
        d2 = json.dumps(d1)     # python object -> json string
        self.assertEqual('[{"a": 1, "b": 2}, {"c": 3, "d": 4, "e": 5}]', d2)

        d1 = '''{
    "a": "python",
    "b": 7
}'''
        d2 = json.dumps({'b': 7, 'a': 'python'}, sort_keys=True, indent=4, separators=(',', ': '))
        self.assertEqual(d1, d2)

        d1 = ["a", 1, 3.14, True, False, None, (1, 2), {"a": 1}]
        d2 = json.dumps(d1)
        self.assertEqual('["a", 1, 3.14, true, false, null, [1, 2], {"a": 1}]', d2)

    def test_loads(self):
        json_data = '{"a":1,"b":2,"c":3,"d":4,"e":5}'
        dic = json.loads(json_data)
        self.assertEqual({'a': 1, 'b': 2, 'c': 3, 'd': 4, 'e': 5}, dic)

        json_data = '["a", 1, 3.14, true, false, null, [1, 2], {"a": 1}]'
        dic = json.loads(json_data)
        self.assertEqual(['a', 1, 3.14, True, False, None, [1, 2], {'a': 1}], dic)


if __name__ == '__main__':
    unittest.main()
