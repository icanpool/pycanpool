#!/usr/bin/env python3
# Copyright (c) 2024 maminjie <canpool@163.com>
# SPDX-License-Identifier: MulanPSL-2.0

import unittest
from typing import List, Tuple, Dict, Mapping, Set, AbstractSet, \
    Sequence, NoReturn, Any, TypeVar, NewType, Callable, Union, \
    Optional, Generator


def f_size(rect: Mapping[str, int]) -> Dict[str, int]:
    return {'width': rect['width'] + 100, 'height': rect['width'] + 100}


def f_describe(s: AbstractSet[int]) -> Set[int]:
    return set(s)


def f_square(elements: Sequence[int]) -> List[int]:
    return [x ** 2 for x in elements]


def f_hello() -> NoReturn:
    print("hello typing module")


def f_add(a: Any):
    return a + 1


def f_date(year: int, month: int, day: int) -> str:
    return f'{year}-{month}-{day}'


def f_get_date_fn() -> Callable[[int, int, int], str]:
    return f_date


def f_process(fn: Union[str, Callable]):
    if isinstance(fn, str):
        return f"process {fn}"
    elif isinstance(fn, Callable):
        return fn()     # arg ??


def f_judge(result: bool) -> Optional[str]:
    """Optional[str] ==> Union[str, None]"""
    if result:
        return 'Error Occurred'


def f_echo_round() -> Generator[int, float, str]:
    """
    YieldType: int
    SendType: float
    ReturnType: str
    """
    sent = 1.4
    while sent:
        sent = yield round(sent)
    return 'Done'


class TestTyping(unittest.TestCase):

    def test_list(self):
        var: List[int or float] = [2, 3.14]
        self.assertEqual([2, 3.14], var)
        var: List[List[int]] = [[1, 2], [2, 3, 4]]
        self.assertEqual([[1, 2], [2, 3, 4]], var)

    def test_tuple(self):
        """Tuple, NamedTuple"""
        person: Tuple[str, int, float] = ('zhang san', 70, 1.75)
        self.assertEqual(70, person[1])

    def test_dict(self):
        """Dict, Mapping, MutableMapping"""
        dic = f_size({'width': 20, 'height': 30})
        self.assertEqual(120, dic['width'])

    def test_set(self):
        """Set, AbstractSet"""
        self.assertEqual({1, 2, 3}, f_describe({3, 2, 1, 2}))

    def test_sequence(self):
        tup = (1, 3, 7)
        self.assertEqual([1, 9, 49], f_square(tup))
        lst = [4, 5]
        self.assertEqual([16, 25], f_square(lst))

    def test_noreturn(self):
        f_hello()

    def test_any(self):
        self.assertEqual(4, f_add(3))
        self.assertEqual(4.9, f_add(3.9))
        self.assertEqual((3+4j), f_add(complex(2, 4)))

    def test_type_var(self):
        # Custom compatible with specific types
        Height = TypeVar('Height', int, float, None)

        def get_height() -> Height:
            return height

        height = 1
        self.assertEqual(1, get_height())
        height = 1.75
        self.assertEqual(1.75, get_height())
        height = None
        self.assertEqual(None, get_height())

    def test_new_type(self):
        Person = NewType('Person', Tuple[str, int, float])  # Person type is tuple
        person = Person(('Mike', 22, 1.75))
        self.assertEqual(1.75, person[2])

    def test_callable(self):
        self.assertTrue(isinstance(f_add, Callable))
        f = f_get_date_fn()
        self.assertEqual("2024-3-24", f(2024, 3, 24))

    def test_union(self):
        self.assertEqual("process hello", f_process('hello'))
        self.assertEqual(None, f_process(f_hello))

    def test_optional(self):
        self.assertEqual(None, f_judge(False))
        self.assertEqual("Error Occurred", f_judge(True))

    def test_generator(self):
        f = f_echo_round()
        self.assertEqual(1, next(f))
        self.assertEqual(2, f.send(1.6))
        self.assertEqual(5, f.send(5.2))
        try:
            print(next(f))
        except StopIteration as e:
            self.assertEqual("Done", str(e))


if __name__ == '__main__':
    unittest.main()
