#!/usr/bin/env python3
# Copyright (c) 2024 maminjie <canpool@163.com>
# SPDX-License-Identifier: MulanPSL-2.0

"""Regular expression"""

import unittest
import re


class TestRegularExpression(unittest.TestCase):
    def test_match(self):
        """Attempts to match a pattern from the beginning of the string"""
        line = "Cats are smarter than dogs"
        match_obj = re.match(r'(.*) are (.*?) .*', line, re.M | re.I)
        if match_obj:
            self.assertEqual(line, match_obj.group())
            self.assertEqual(('Cats', 'smarter'), match_obj.groups())
            self.assertEqual('Cats', match_obj.group(1))
            self.assertEqual('smarter', match_obj.group(2))
            try:
                match_obj.group(3)
            except IndexError as e:
                self.assertEqual("no such group", str(e))

        match_obj = re.match(r'dogs', line, re.M | re.I)
        self.assertEqual(None, match_obj)

    def test_search(self):
        """Scans the entire string and returns the first successful match"""
        line = "Cats are smarter than dogs"
        search_obj = re.search(r'(.*) are (.*?) .*', line, re.M | re.I)
        if search_obj:
            self.assertEqual(line, search_obj.group())
            self.assertEqual(('Cats', 'smarter'), search_obj.groups())
            self.assertEqual(2, len(search_obj.groups()))
            self.assertEqual('Cats', search_obj.group(1))
            self.assertEqual('smarter', search_obj.group(2))
            try:
                search_obj.group(3)
            except IndexError as e:
                self.assertEqual("no such group", str(e))

        search_obj = re.search(r'dogs', line, re.M | re.I)
        self.assertNotEqual(None, search_obj)
        self.assertEqual('dogs', search_obj.group())
        self.assertEqual((), search_obj.groups())

    def test_sub(self):
        phone = "2004-959-559 # this is phone number"
        num = re.sub(r'#.*$', "", phone)    # remove comment
        self.assertEqual("2004-959-559 ", num)
        num = re.sub(r'\D', "", phone)
        self.assertEqual("2004959559", num)

        def double(matched):
            value = int(matched.group('value'))
            return str(value * 2)

        s = 'A23G4HFD567'
        s = re.sub(r'(?P<value>\d+)', double, s)    # ??
        self.assertEqual("A46G8HFD1134", s)

    def test_compile(self):
        p = re.compile(r'\d+')
        m = p.match('one12twothree34four')
        self.assertEqual(None, m)
        m = p.match('one12twothree34four', 2, 10)
        self.assertEqual(None, m)
        m = p.match('one12twothree34four', 3, 10)
        self.assertEqual('12', m.group())
        self.assertEqual('12', m.group(0))
        self.assertEqual(3, m.start())
        self.assertEqual(5, m.end())
        self.assertEqual((3, 5), m.span())  # [3, 5)

        p = re.compile(r'([a-z]+) ([a-z]+)', re.I)
        m = p.match('Hello World Wide Web')
        self.assertNotEqual(None, m)
        self.assertEqual('Hello World', m.group(0))
        self.assertEqual((0, 11), m.span(0))
        self.assertEqual('Hello', m.group(1))
        self.assertEqual((0, 5), m.span(1))
        self.assertEqual('World', m.group(2))
        self.assertEqual((6, 11), m.span(2))
        self.assertEqual(('Hello', 'World'), m.groups())

    def test_findall(self):
        s = 'baidu 123 google 456'
        r = re.findall(r'\d+', s)
        self.assertEqual(['123', '456'], r)
        p = re.compile(r'\d+')
        r = p.findall(s)
        self.assertEqual(['123', '456'], r)
        r = p.findall('bai88du123google456', 0, 9)
        self.assertEqual(['88', '12'], r)

        r = re.findall(r'(\w+)=(\d+)', 'set width=20 and height=10')
        self.assertEqual([('width', '20'), ('height', '10')], r)

    def test_finditer(self):
        it = re.finditer(r"\d+","12a32bc43jf3")
        s = ""
        for m in it:
            s += m.group() + ','
        self.assertEqual("12,32,43,3,", s)

    def test_split(self):
        r = re.split(r'\W+', 'a, a, a.')
        self.assertEqual(['a', 'a', 'a', ''], r)
        r = re.split(r'\W+', ' a, a, a.')
        self.assertEqual(['', 'a', 'a', 'a', ''], r)
        r = re.split(r'(\W+)', ' a, a, a.')     # Also return the split character [' ', ', ', ', ', '.']
        self.assertEqual(['', ' ', 'a', ', ', 'a', ', ', 'a', '.', ''], r)
        r = re.split(r'\W+', ' a, a, a.', 1)
        self.assertEqual(['', 'a, a, a.'], r)

        self.assertEqual(['hello world'], re.split('a+', 'hello world'))

    def test_pattern(self):
        s = "abbbc"
        m = re.search("^a", s)
        self.assertEqual('a', m.group())
        m = re.search(r'bc$', s)
        self.assertEqual('bc', m.group())
        m = re.search("a.b", s)
        self.assertEqual('abb', m.group())
        m = re.search("[ac]", s)
        self.assertEqual('a', m.group())
        m = re.search("[cb]", s)
        self.assertEqual('b', m.group())
        m = re.search("[^ab]", s)
        self.assertEqual('c', m.group())
        m = re.search("ab*", s)
        self.assertEqual('abbb', m.group())
        m = re.search("ac*", s)
        self.assertEqual('a', m.group())
        m = re.search("ab+", s)
        self.assertEqual('abbb', m.group())
        m = re.search("ac+", s)
        self.assertEqual(None, m)
        m = re.search("ab?", s)
        self.assertEqual('ab', m.group())
        m = re.search("ac?", s)
        self.assertEqual('a', m.group())
        m = re.search("ac?", s)
        self.assertEqual('a', m.group())
        m = re.search("ab{2}", s)
        self.assertEqual('abb', m.group())
        m = re.search("ab{4}", s)
        self.assertEqual(None, m)
        m = re.search("ab{2,}", s)
        self.assertEqual('abbb', m.group())
        m = re.search("ab{2,4}", s)
        self.assertEqual('abbb', m.group())
        m = re.search("a|b", s)
        self.assertEqual('a', m.group())
        m = re.search("(ab).*(bc)", s)
        self.assertEqual("abbbc", m.group())
        self.assertEqual(('ab', 'bc'), m.groups())

        s = "bAABA"
        m = re.search("(?im:BA)", s)
        self.assertEqual("bA", m.group())
        m = re.search("(?-im:BA)", s)
        self.assertEqual("BA", m.group())

        s = "aab_ABB, ,1-2"
        m = re.search(r'\w+', s)
        self.assertEqual("aab_ABB", m.group())
        m = re.search(r'\W+', s)
        self.assertEqual(", ,", m.group())
        m = re.search(r'\s+', s)
        self.assertEqual(" ", m.group())
        m = re.search(r'\S+', s)
        self.assertEqual("aab_ABB,", m.group())
        m = re.search(r'\d+', s)
        self.assertEqual("1", m.group())
        m = re.search(r'[0-9]+', s)
        self.assertEqual("1", m.group())


if __name__ == '__main__':
    unittest.main()
