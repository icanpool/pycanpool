#!/usr/bin/env python3
# Copyright (c) 2024 maminjie <canpool@163.com>
# SPDX-License-Identifier: MulanPSL-2.0

import unittest
from collections import OrderedDict


class TestCollections(unittest.TestCase):
    """test stdlib collections"""

    def test_ordered_dict(self):
        od = OrderedDict()
        od['a'] = 1
        od['d'] = 4
        od['b'] = 3
        self.assertEqual(('a', 'd', 'b'), tuple(od.keys()))
        self.assertEqual([1, 4, 3], list(od.values()))
        
        x = ""
        for k, v in od.items():
            x += k + str(v)
        self.assertEqual("a1d4b3", x)


if __name__ == '__main__':
    unittest.main()
