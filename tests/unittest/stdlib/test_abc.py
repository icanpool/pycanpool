#!/usr/bin/env python3
# Copyright (c) 2024 maminjie <canpool@163.com>
# SPDX-License-Identifier: MulanPSL-2.0

import unittest
from abc import ABC, abstractmethod


class Talker(ABC):
    """abstract base class"""
    @abstractmethod
    def talk(self):
        pass


class Knigget(Talker):
    def talk(self):
        return "Ni"


class TestAbstractClass(unittest.TestCase):
    """test abstract base class"""

    def test_class(self):
        # t = Talker()      # TypeError: Can't instantiate abstract class Talker with abstract method talk
        k = Knigget()
        self.assertTrue(isinstance(k, Talker))
        self.assertEqual("Ni", k.talk())


if __name__ == '__main__':
    unittest.main()
